#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <inttypes.h>
#include <signal.h>
#include <libvmi/libvmi.h>
#include <libvmi/events.h>


int main (int argc, char **argv)
{
    vmi_instance_t vmi;
    unsigned char *memory = NULL;
    uint32_t offset;
    addr_t list_head = 0, next_list_entry = 0, previous_next_ps_entry = 0, next_previous_ps_entry = 0, next_ps_entry = 0, previous_ps_entry = 0, list_head1 = 0, session_listhead = 0;
    addr_t current_process = 0;
    addr_t tmp_next = 0;
    addr_t next_module = 0;
    addr_t eprocess_head = 0;
    addr_t peb_listhead =  0;
    addr_t teb_addr =  0;
    addr_t initial_stack_base_addr = 0;
    addr_t ethread= 0;
    addr_t kthread=0;
    addr_t tib_addr = 0;
    unsigned long sessionptr_offset = 0,slink_offset = 0;
    char *procname = NULL;
    vmi_pid_t pid = 0;
    unsigned long initorder = 0,peb_offset = 0,tasks_offset = 0, pid_offset = 0, name_offset = 0, handletable_offset = 0,ht_offset = 0,ht_pid = 0,eprocess_offset = 0;
    unsigned long stack_limit_offset = 0, initial_stack_base=0, stack_limit=0, threadlisthead_offset=0, ethread_offset = 0, tib_offset = 0, kthread_offset = 0, teb_offset = 0xfe4, session_offset = 0,peb_ldr_offset = 0,taskrun_offset = 0;
    status_t status;
    int ret_val = 0; // return code for after goto
    struct sigaction signal_action;

    const char*       sym;
    uint16_t    *off;
    addr_t      *add;
    uint8_t     *byt;

    /* this is the VM or file that we are looking at */
    if (argc != 3) {
        printf("Usage: %s <vmname> pid\n", argv[0]);
        //return 1;
    }

    char *name = argv[1];

    /* initialize the libvmi library */


    if (VMI_FAILURE ==  vmi_init_complete(&vmi, name, VMI_INIT_DOMAINNAME, NULL, VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL))
    {
        printf("Failed to init LibVMI library.\n");
        return 1;
    }

    /* init the offset values */
    if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
        tasks_offset = vmi_get_offset(vmi, "win_tasks");
        name_offset = vmi_get_offset(vmi, "win_pname");
        pid_offset = vmi_get_offset(vmi, "win_pid");
       
        
    }

    if (0 == tasks_offset) {
        printf("Failed to find win_tasks\n");
        goto error_exit;
    }
    if (0 == pid_offset) {
        printf("Failed to find win_pid\n");
        goto error_exit;
    }
    if (0 == name_offset) {
        printf("Failed to find win_pname\n");
        goto error_exit;
    }

    /* pause the vm for consistent memory access */
    
    
    /* demonstrate name and id accessors */
    char *name2 = vmi_get_name(vmi);

    // if (VMI_FILE != vmi_get_access_mode(vmi)) {
    //     uint64_t id = vmi_get_vmid(vmi);

    //     printf("Process listing for VM %s (id=%"PRIu64")\n", name2, id);
    // }
    // else {
    //     printf("Process listing for file %s\n", name2);
    // }
    // free(name2);

    /* get the head of the list */
    if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
        /* Begin at PID 0, the 'swapper' task. It's not typically shown by OS
         *  utilities, but it is indeed part of the task list and useful to
         *  display as such.
         */
        list_head = vmi_translate_ksym2v(vmi, "init_task") + tasks_offset;
    }
    else if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
       
        if(VMI_FAILURE == vmi_read_addr_ksym(vmi, "PsActiveProcessHead", &list_head)) {
            printf("Failed to find PsActiveProcessHead\n");
            goto error_exit;
        }
        
        status = vmi_read_addr_va(vmi, list_head - tasks_offset + handletable_offset, 0, &list_head1);
        if (status == VMI_FAILURE) {
            printf("Failed to read next pointer in loop at %"PRIx64"\n", next_list_entry);
            goto error_exit;
        }
        status = vmi_read_addr_va(vmi, list_head - tasks_offset + session_offset, 0, &session_listhead);
        if (status == VMI_FAILURE) {
            printf("Failed to read next pointer in loop at %"PRIx64"\n", session_listhead);
            goto error_exit;
        }
        status = vmi_read_addr_va(vmi, list_head - tasks_offset + peb_offset, 0, &peb_listhead);
        if (status == VMI_FAILURE) {
            printf("Failed to read next pointer in loop at %"PRIx64"\n", peb_listhead);
            goto error_exit;
        }
    

    //intf("Printing the main process list\n");
    
    next_list_entry = list_head;
      //add_breakpoint("vulnerable_function", 1, 0, get_nbytes_buf);
    /* walk the task list */
        do { 

        current_process = next_list_entry - tasks_offset;

        /* Note: the task_struct that we are looking at has a lot of
         * information.  However, the process name and id are burried
         * nice and deep.  Instead of doing something sane like mapping
         * this data to a task_struct, I'm just jumping to the location
         * with the info that I want.  This helps to make the example
         * code cleaner, if not more fragile.  In a real app, you'd
         * want to do this a little more robust :-)  See
         * include/linux/sched.h for mode details */

        /* NOTE: _EPROCESS.UniqueProcessId is a really VOID*, but is never > 32 bits,
         * so this is safe enough for x64 Windows for example purposes */
        

        vmi_read_32_va(vmi, current_process + pid_offset, 0, (uint32_t*)&pid);
        //printf("%d\n", pid);

        //printf("Currently checking PID: [%d] \n", pid);
          //add_breakpoint("vulnerable_function", 1, 0, get_nbytes_buf);
         // add_breakpoint("vulnerable_function", 2, 0, after_extract_buf);

        if (pid == (vmi_pid_t)atoi(argv[2])){
            
            uint32_t change;
            //address of the not_called function in the example
            change = 0x00401340;
            vmi_pid_t a = 0;
            char * buffer;
            buffer = (char *) malloc (1024);
            printf("Before Modicication\n");
            //Location of not called function in example buffer overflow program Its long because the host VM is 64 bits
            vmi_read_va(vmi, 0x000000000028fe7c, pid, buffer, 16); 
            vmi_print_hex(buffer, 16);

            //pause VM for consistent access 
            vmi_pause_vm(vmi);
            status = vmi_write_32_va(vmi, (addr_t )0x000000000028fe7c, pid, (uint32_t*)&change);

            //free(change);
            vmi_resume_vm(vmi);
            printf("After Modicication\n");
            //After Modification
            vmi_read_va(vmi, 0x000000000028fe7c, pid, buffer, 16); 
            vmi_print_hex(buffer, 16);
            printf("\n");
            break;
        }

        //goto next ptr        
        status = vmi_read_addr_va(vmi, next_list_entry, 0, &next_list_entry);
        if (status == VMI_FAILURE) {
            printf("Failed to read next pointer in loop at %"PRIx64"\n", next_list_entry);
            goto error_exit;
        }

        } while(next_list_entry != list_head);

    }else{
        printf("PID != ATOI" );

    }
error_exit:

    // resume the vm
    printf("Resuming the VM\n");
    vmi_resume_vm(vmi);

    // cleanup any memory associated with the LibVMI instance
    printf("Cleaning up\n");
    vmi_destroy(vmi);

    return ret_val;

    return 0;
}