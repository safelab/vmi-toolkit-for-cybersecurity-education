git clone https://github.com/libvmi/libvmi.git;
cd libvmi;
sudo apt-get install libxc-dev libxen-dev qemu-kvm libvirt-bin bison flex libglib*;
./autogen.sh;
./configure --disable-kvm;
sudo make install;
